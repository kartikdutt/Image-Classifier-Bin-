from keras.models import Sequential
from keras.layers import Dense,Convolution2D,MaxPooling2D,Flatten
clas=Sequential()
clas.add(Convolution2D(32,3,3,input_shape=(64,64,3), activation='relu'))
clas.add(MaxPooling2D(pool_size=(2,2)))
clas.add(Convolution2D(64,3,3,activation='relu'))
clas.add(MaxPooling2D(pool_size=(2,2)))
clas.add(Flatten())
clas.add(Dense(output_dim=256,activation='relu'))
clas.add(Dense(output_dim=1,activation='sigmoid'))
clas.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy'])
from keras.preprocessing.image import ImageDataGenerator
train_data=ImageDataGenerator(
            rescale=1./255,
            shear_range=0.2,
            zoom_range=0.2,
            horizontal_flip=True)
test_data=ImageDataGenerator(rescale=1./255)
training_set=train_data.flow_from_directory('dataset/training_set',
                                            target_size=(64,64),
                                            batch_size=32,
                                            class_mode='binary')
test_set=train_data.flow_from_directory('dataset/training_set',
                                            target_size=(64,64),
                                            batch_size=32,
                                            class_mode='binary')
clas.fit_generator(training_set,
                    samples_per_epoch=8000,
                    nb_epoch=25,
                    validation_data=test_set,
                    nb_val_samples=2000)
import numpy as np
from keras.preprocessing import image
test_img=image.load_img('dataset/single_prediction/cat_or_dog_2.jpg',target_size=(64,64))
test_img=image.img_to_array(test_img)
test_img=np.expand_dims(test_img,axis=0) 
result=clas.predict(test_img)
if result[0]==1:
    print("Dog")
else:
    print("Cat")